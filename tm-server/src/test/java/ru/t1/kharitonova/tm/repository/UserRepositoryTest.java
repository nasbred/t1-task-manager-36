package ru.t1.kharitonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        userRepository = new UserRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user_" + i);
            user.setEmail("user_" + i + "@test.email");
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @After
    public void clearRepository() {
        userList.clear();
        userRepository.removeAll();
    }

    @Test
    public void testAdd() {
        @NotNull final User user = new User();
        userRepository.add(user);
        Assert.assertEquals(user, userRepository.findById(user.getId()));
    }

    @Test
    public void testAddAll() {
        @NotNull List<User> userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user_add_" + i);
            user.setRole(Role.USUAL);
        }
        userRepository.addAll(userList);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            Assert.assertTrue(userRepository.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertFalse(userRepository.existsById(id));
    }

    @Test
    public void testExistByIdForUserPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String id = user.getId();
            Assert.assertTrue(userRepository.existsById(id));
        }
    }

    @Test
    public void testExistByIdForUserNegative() {
        for (@NotNull final User user : userList) {
            @NotNull final String id = UUID.randomUUID().toString();
            Assert.assertFalse(userRepository.existsById(id));
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findOneById(user.getId()));
            Assert.assertEquals(user, userRepository.findOneById(user.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(userRepository.findOneById(id));
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Assert.assertEquals(userList.get(i), userRepository.findOneByIndex(i));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() throws IndexOutOfBoundsException {
        userRepository.findOneByIndex(NUMBER_OF_ENTRIES + 1);
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> actualUserList = userRepository.findAll();
        Assert.assertEquals(userList, actualUserList);
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }


    @Test
    public void testRemovePositive() {
        for (@NotNull final User user : userList) {
            userRepository.remove(user);
            Assert.assertNull(userRepository.findOneById(user.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final User user = new User();
        userRepository.remove(user);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testRemoveOnePositive() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = userList.get(i);
            userRepository.removeOneById(user.getId());
            Assert.assertEquals(userRepository.getSize(), NUMBER_OF_ENTRIES - i - 1);
        }
    }

    @Test
    public void testRemoveOneNegative() {
        @NotNull final User user = new User();
        userRepository.removeOneById(user.getId());
        Assert.assertEquals(userRepository.getSize(), NUMBER_OF_ENTRIES);
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        userRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.removeOneById(user.getId()));
            Assert.assertNull(userRepository.findOneById(user.getId()));
        }
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(userRepository.removeOneById(id));
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }
}
