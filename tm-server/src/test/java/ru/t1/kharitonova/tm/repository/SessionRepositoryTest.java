package ru.t1.kharitonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.kharitonova.tm.api.repository.ISessionRepository;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final String userId = i <= NUMBER_OF_ENTRIES / 2 ? USER_ID_1 : USER_ID_2;
            @NotNull final Session session = new Session(userId, Role.USUAL);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @After
    public void clearRepository() {
        sessionList.clear();
        sessionRepository.removeAll();
    }

    @Test
    public void testAdd() {
        @NotNull final Session session = new Session(USER_ID_1, Role.USUAL);
        sessionRepository.add(session);
        Assert.assertTrue(sessionRepository.existsById(session.getId()));
    }

    @Test
    public void testAddAll() {
        @NotNull List<Session> sessionList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) sessionList.add(new Session());
        sessionRepository.addAll(sessionList);
        Assert.assertEquals(NUMBER_OF_ENTRIES * 2, sessionRepository.getSize());
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionRepository.findOneById(session.getId()));
            Assert.assertEquals(session, sessionRepository.findOneById(session.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findOneById(id));
    }

    @Test
    public void testFindOneByIdForUser() {
        for (@NotNull final Session session : sessionList) {
            if (session.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(sessionRepository.findOneById(USER_ID_1, session.getId()));
                Assert.assertEquals(session, sessionRepository.findOneById(USER_ID_1, session.getId()));
            } else Assert.assertNull(sessionRepository.findOneById(USER_ID_1, session.getId()));
        }
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Assert.assertEquals(sessionList.get(i), sessionRepository.findOneByIndex(i));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() throws IndexOutOfBoundsException {
        sessionRepository.findOneByIndex(NUMBER_OF_ENTRIES + 1);
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final Session expected1 = sessionList.get(0);
        @NotNull final Session expected2 = sessionList.get(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(expected1, sessionRepository.findOneByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, sessionRepository.findOneByIndex(USER_ID_2, 0));
        Assert.assertNull(sessionRepository.findOneByIndex("unknown_user_id", 0));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> actualSessionList = sessionRepository.findAll();
        Assert.assertEquals(sessionList, actualSessionList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> actualSessionList = sessionRepository.findAll(USER_ID_1);
        Assert.assertEquals(sessionList.subList(0, NUMBER_OF_ENTRIES / 2), actualSessionList);
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Session session : sessionList) {
            sessionRepository.remove(session);
            Assert.assertNull(sessionRepository.findOneById(session.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Session session = new Session();
        sessionRepository.remove(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testRemoveOnePositive() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = sessionList.get(i);
            sessionRepository.removeOne(session.getUserId(), session);
            Assert.assertEquals(sessionRepository.getSize(), NUMBER_OF_ENTRIES - i - 1);
        }
    }

    @Test
    public void testRemoveOneNegative() {
        @NotNull final Session session = new Session();
        sessionRepository.removeOne(USER_ID_1, session);
        Assert.assertEquals(sessionRepository.getSize(), NUMBER_OF_ENTRIES);
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        sessionRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveAllForUserPositive() {
        @NotNull final List<Session> emptyList = new ArrayList<>();
        sessionRepository.removeAllByUserId(USER_ID_1);
        Assert.assertEquals(emptyList, sessionRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(emptyList, sessionRepository.findAll(USER_ID_2));
    }

    @Test
    public void testRemoveAllForUserNegative() {
        sessionRepository.removeAllByUserId("Other_user_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final Session session : sessionList) {
            Assert.assertNotNull(sessionRepository.removeOneById(session.getId()));
            Assert.assertNull(sessionRepository.findOneById(session.getId()));
        }
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.removeOneById(id));
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testRemoveOneByIndexPositive() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            sessionRepository.removeOneByIndex(0);
            Assert.assertEquals(NUMBER_OF_ENTRIES - i, sessionRepository.getSize());
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveOneByIndexNegative() {
        sessionRepository.removeOneByIndex(NUMBER_OF_ENTRIES + 1);
    }

}
