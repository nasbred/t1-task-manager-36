package ru.t1.kharitonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID_2 = UUID.randomUUID().toString();


    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task # " + i);
            task.setDescription("Description # " + i);
            if (i <= NUMBER_OF_ENTRIES / 2) {
                task.setUserId(USER_ID_1);
                task.setProjectId(PROJECT_ID_1);
            } else {
                task.setUserId(USER_ID_2);
                task.setProjectId(PROJECT_ID_2);
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @After
    public void clearRepository() {
        taskList.clear();
        taskRepository.removeAll();
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task task = new Task();
        @NotNull final String userId = USER_ID_1;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Task Description";
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task actualTask = taskRepository.findOneById(task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Task> taskList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task TEST ADD # " + i);
            task.setDescription("Description Task TEST ADD #" + i);
            task.setUserId(USER_ID_2);
            taskList.add(task);
        }
        taskRepository.addAll(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            Assert.assertTrue(taskRepository.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertFalse(taskRepository.existsById(id));
    }

    @Test
    public void testExistByIdForUserPositive() {
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            Assert.assertTrue(taskRepository.existsById(userId, id));
        }
    }

    @Test
    public void testExistByIdForUserNegative() {
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = UUID.randomUUID().toString();
            Assert.assertFalse(taskRepository.existsById(userId, id));
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.findOneById(task.getId()));
            Assert.assertEquals(task, taskRepository.findOneById(task.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findOneById(id));
    }

    @Test
    public void testFindOneByIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(taskRepository.findOneById(USER_ID_1, task.getId()));
                Assert.assertEquals(task, taskRepository.findOneById(USER_ID_1, task.getId()));
            } else Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
        }
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Assert.assertEquals(taskList.get(i), taskRepository.findOneByIndex(i));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() throws IndexOutOfBoundsException {
        taskRepository.findOneByIndex(NUMBER_OF_ENTRIES + 1);
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(expected1, taskRepository.findOneByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, taskRepository.findOneByIndex(USER_ID_2, 0));
        Assert.assertNull(taskRepository.findOneByIndex("unknown_user_id", 0));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> actualTaskList = taskRepository.findAll();
        Assert.assertEquals(taskList, actualTaskList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> actualTaskList = taskRepository.findAll(USER_ID_1);
        Assert.assertEquals(taskList.subList(0, NUMBER_OF_ENTRIES / 2), actualTaskList);
    }

    @Test
    public void testFindAllByProjectIdPositive() {
        @NotNull final List<Task> taskList1 = taskList.stream()
                .filter(m -> PROJECT_ID_1.equals(m.getProjectId()))
                .filter(m -> USER_ID_1.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Task> taskList2 = taskList.stream()
                .filter(m -> PROJECT_ID_2.equals(m.getProjectId()))
                .filter(m -> USER_ID_2.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Task> actualTaskList1 = taskRepository.findAllByProjectId(
                USER_ID_1, PROJECT_ID_1);
        @NotNull final List<Task> actualTaskList2 = taskRepository.findAllByProjectId(
                USER_ID_2, PROJECT_ID_2);
        Assert.assertEquals(taskList1, actualTaskList1);
        Assert.assertEquals(taskList2, actualTaskList2);
    }

    @Test
    public void testFindAllByProjectIdNegative() {
        @NotNull final List<Task> emptyList = new ArrayList<>();
        @NotNull final List<Task> actualTaskList = taskRepository.findAllByProjectId(
                "Other_user_id", "Other_project_id");
        Assert.assertEquals(emptyList, actualTaskList);
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Task task : taskList) {
            taskRepository.remove(task);
            Assert.assertNull(taskRepository.findOneById(task.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Task task = new Task();
        taskRepository.remove(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testRemoveOnePositive() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = taskList.get(i);
            taskRepository.removeOne(task.getUserId(), task);
            Assert.assertEquals(taskRepository.getSize(), NUMBER_OF_ENTRIES - i - 1);
        }
    }

    @Test
    public void testRemoveOneNegative() {
        @NotNull final Task task = new Task();
        taskRepository.removeOne(USER_ID_1, task);
        Assert.assertEquals(taskRepository.getSize(), NUMBER_OF_ENTRIES);
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        taskRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveAllForUserPositive() {
        @NotNull final List<Task> emptyList = new ArrayList<>();
        taskRepository.removeAllByUserId(USER_ID_1);
        Assert.assertEquals(emptyList, taskRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(emptyList, taskRepository.findAll(USER_ID_2));
    }

    @Test
    public void testRemoveAllForUserNegative() {
        taskRepository.removeAllByUserId("Other_user_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.removeOneById(task.getId()));
            Assert.assertNull(taskRepository.findOneById(task.getId()));
        }
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.removeOneById(id));
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testRemoveOneByIndexPositive() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            taskRepository.removeOneByIndex(0);
            Assert.assertEquals(NUMBER_OF_ENTRIES - i, taskRepository.getSize());
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveOneByIndexNegative() {
        taskRepository.removeOneByIndex(NUMBER_OF_ENTRIES + 1);
    }

}
