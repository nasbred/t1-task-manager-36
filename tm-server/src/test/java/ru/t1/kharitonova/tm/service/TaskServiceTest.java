package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.api.service.IProjectService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.ITaskService;
import ru.t1.kharitonova.tm.api.service.IUserService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.exception.entity.TaskNotFoundException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.NameEmptyException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;
import ru.t1.kharitonova.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @NotNull
    private List<Project> projectList;

    @NotNull
    private List<Task> taskList;

    @NotNull
    private List<User> userList;

    @Before
    public void init() {
        projectList = new ArrayList<>();
        taskList = new ArrayList<>();
        userList = new ArrayList<>();

        @NotNull final User userAdmin = new User();
        @NotNull final String adminId = userAdmin.getId();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setEmail("admin@test.ru");
        userAdmin.setRole(Role.ADMIN);

        @NotNull final User userTest = new User();
        @NotNull final String userId = userTest.getId();
        userTest.setLogin("test");
        userTest.setPasswordHash("test");
        userTest.setEmail("test@test.ru");
        userTest.setRole(Role.USUAL);

        userService.add(userAdmin);
        userService.add(userTest);
        userList.add(userAdmin);
        userList.add(userTest);

        projectList.add(new Project(adminId, "Project 1", "Description 1 admin", Status.IN_PROGRESS));
        projectList.add(new Project(adminId, "Project 2", "Description 2 admin", Status.NOT_STARTED));
        projectList.add(new Project(userId, "Project 3", "Description 3 user", Status.IN_PROGRESS));
        projectList.add(new Project(userId, "Project 4", "Description 4 user", Status.NOT_STARTED));

        taskList.add(new Task(adminId, "Task 1", "Description 1 admin", Status.NOT_STARTED, projectList.get(0).getId()));
        taskList.add(new Task(adminId, "Task 2", "Description 2 admin", Status.IN_PROGRESS, projectList.get(1).getId()));
        taskList.add(new Task(userId, "Task 3", "Description 3 user", Status.IN_PROGRESS, projectList.get(2).getId()));
        taskList.add(new Task(userId, "Task 4", "Description 4 user", Status.IN_PROGRESS, null));

        for (@NotNull final Project project : projectList) projectService.add(project);
        for (@NotNull final Task task : taskList) taskService.add(task);
    }

    @After
    public void clear() {
        projectList.clear();
        taskList.clear();
        userList.clear();
    }

    @Test
    public void testAdd() {
        @NotNull final Task task = new Task(
                userList.get(0).getId(),
                "Test Task",
                "Test Add Task",
                Status.NOT_STARTED,
                null);
        taskService.add(task);
        Assert.assertTrue(taskService.existsById(task.getId()));
    }

    @Test
    public void testAddAll() {
        @NotNull List<Task> tasks = new ArrayList<>();
        final int taskSize = taskService.getSize();
        for (int i = 0; i < taskSize; i++) {
            tasks.add(new Task());
        }
        taskService.addAll(tasks);
        Assert.assertEquals(taskSize * 2, taskService.getSize());
    }

    @Test
    public void testCreate() {
        int taskSize = taskList.size();
        int i = 0;
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final String name = "Task" + user.getLogin();
            @NotNull final String description = "Description_" + user.getLogin();
            @NotNull final Task task = taskService.create(name, userId, description);
            Assert.assertNotNull(task);
            i++;
        }
        Assert.assertEquals(taskSize + i, taskService.getSize());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithNullUser() {
        Assert.assertNotNull(taskService.create(
                null,
                "Test Task",
                "Test Description")
        );
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithNullName() {
        Assert.assertNotNull(taskService.create(
                userList.get(0).getId(),
                null,
                "Test Description")
        );
    }

    @Test
    public void testChangeStatusByIdPositive() {
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            taskService.changeTaskStatusById(userId, id, Status.COMPLETED);
            @Nullable final Task actualTask = taskService.findOneById(userId, id);
            Assert.assertNotNull(actualTask);
            @NotNull final Status actualStatus = actualTask.getStatus();
            Assert.assertEquals(Status.COMPLETED, actualStatus);
        }
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusByIdNegative() {
        @NotNull final String userId = "Other_user_id";
        @NotNull final String id = "Other_id";
        taskService.changeTaskStatusById(userId, id, Status.COMPLETED);
    }

    @Test
    public void testChangeStatusByIndexPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> userTasks = taskService.findAll(userId);
            for (int i = 0; i < userTasks.size(); i++) {
                @Nullable final Task task = taskService.findOneByIndex(userId, i);
                taskService.changeTaskStatusByIndex(userId, i, Status.IN_PROGRESS);
                Assert.assertNotNull(task);
                @NotNull final Status status = task.getStatus();
                Assert.assertEquals(Status.IN_PROGRESS, status);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIndexNegative() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> userTasks = taskService.findAll(userId);
            taskService.changeTaskStatusByIndex(userId, userTasks.size() + 1, Status.IN_PROGRESS);
        }
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            Assert.assertTrue(taskService.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertFalse(taskService.existsById("Other_Id"));
        Assert.assertFalse(taskService.existsById("Other_user_id", "Other_Id"));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(taskList.size(), tasks.size());
    }

    @Test
    public void testFindAllByUserId() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> actualTasks = taskService.findAll(userId);
            @NotNull final List<Task> expectedTasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            Assert.assertEquals(expectedTasks, actualTasks);
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            Assert.assertEquals(task, taskService.findOneById(id));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = "Other_id";
        @Nullable final Task task = taskService.findOneById(id);
        Assert.assertNull(task);
    }

    @Test
    public void testFindOneByIdUserPositive() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            @Nullable final String userId = task.getUserId();
            Assert.assertEquals(task, taskService.findOneById(userId, id));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final String id = "Other_Id";
        @NotNull final String userId = "Other_UserId";
        @Nullable final Task task = taskService.findOneById(userId, id);
        Assert.assertNull(task);
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < taskList.size(); i++) {
            @Nullable final Task actualTask = taskService.findOneByIndex(i);
            @Nullable final Task expectedTask = taskList.get(i);
            Assert.assertNotNull(actualTask);
            Assert.assertEquals(expectedTask, actualTask);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindOneByIndexNegative() {
        final int index = taskList.size() + 1;
        Assert.assertNull(taskService.findOneByIndex(index));
    }

    @Test
    public void testFindByIndexForUserPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> tasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < tasks.size(); i++) {
                Assert.assertNotNull(taskService.findOneByIndex(userId, i));
                @Nullable final Task actualTask = taskService.findOneByIndex(userId, i);
                Assert.assertNotNull(actualTask);
                @Nullable final Task expectedTask = tasks.get(i);
                Assert.assertEquals(expectedTask, actualTask);
            }
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIndexForUserNegative() {
        @NotNull final String userId = "Other_User_Id";
        final int index = taskList.size() + 1;
        Assert.assertNull(taskService.findOneByIndex(userId, index));
    }

    @Test
    public void testGetSize() {
        final int projectSize = taskService.getSize();
        Assert.assertEquals(taskList.size(), projectSize);
    }

    @Test
    public void testGetSizeForUser() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            final int actualProjectSize = taskService.getSize(userId);
            @NotNull final List<Task> expectedTasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            final int expectedTaskSize = expectedTasks.size();
            Assert.assertEquals(expectedTaskSize, actualProjectSize);
        }
    }

    @Test
    public void testRemoveOne() {
        for (int i = 0; i < taskList.size(); i++) {
            @NotNull final Task task = taskList.get(i);
            taskRepository.removeOne(task.getUserId(), task);
            Assert.assertEquals(taskRepository.getSize(), taskList.size() - i - 1);
        }
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String id = task.getId();
            Assert.assertNotNull(taskService.removeOneById(id));
        }
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = "Other_Id";
        Assert.assertNull(taskService.removeOneById(id));
        Assert.assertEquals(taskList.size(), taskService.getSize());
    }

    @Test
    public void testRemoveOneByIndexPositive() {
        for (int i = 0; i < taskList.size(); i++) {
            Assert.assertNotNull(taskService.removeOneByIndex(0));
        }
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIndexNegative() {
        @NotNull final Integer index = taskList.size() + 1;
        Assert.assertNull(taskService.removeOneByIndex(index));
    }

    @Test
    public void testRemoveAll() {
        taskService.removeAll();
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveAllByUserIdPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            taskService.removeAllByUserId(userId);
            Assert.assertEquals(0, taskService.getSize(userId));
        }
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testRemoveAllByUserIdNegative() {
        taskService.removeAllByUserId("Other_user_id");
        Assert.assertEquals(taskList.size(), taskService.getSize());
    }

    @Test
    public void testUpdateByIdPositive() {
        for (@NotNull final Task task : taskList) {
            @Nullable final String userId = task.getUserId();
            @NotNull final String id = task.getId();
            @NotNull final String name = "Task Test Update " + id;
            @NotNull final String description = "Description Test Update " + id;
            @Nullable final Task actualTask = taskService.updateById(userId, id, name, description);
            Assert.assertEquals(name, actualTask.getName());
            Assert.assertEquals(description, actualTask.getDescription());
            Assert.assertEquals(task, actualTask);
        }
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateByIdNegative() {
        @NotNull final String userId = "Other_User_Id";
        @NotNull final String id = taskList.get(0).getId();
        @NotNull final String name = "Task Test Update " + id;
        @NotNull final String description = "Description Test Update " + id;
        Assert.assertNotNull(taskService.updateById(userId, id, name, description));
    }

    @Test
    public void testUpdateByIndexPositive() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @NotNull final List<Task> tasks = taskList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            for (int i = 0; i < tasks.size(); i++) {
                @NotNull final String name = "Project Test Update " + i;
                @NotNull final String description = "Description Test Update " + i;
                Assert.assertNotNull(taskService.findOneByIndex(userId, i));
                @Nullable final Task actualTask = taskService.updateByIndex(userId, i, name, description);
                Assert.assertEquals(name, actualTask.getName());
                Assert.assertEquals(description, actualTask.getDescription());
                Assert.assertEquals(tasks.get(i), actualTask);
            }
        }
    }

}
