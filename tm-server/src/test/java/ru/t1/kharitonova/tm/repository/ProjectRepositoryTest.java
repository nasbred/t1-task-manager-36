package ru.t1.kharitonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project # " + i);
            project.setDescription("Description # " + i);
            if (i <= NUMBER_OF_ENTRIES / 2) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @After
    public void clearRepository() {
        projectList.clear();
        projectRepository.removeAll();
    }

    @Test
    public void testAdd() {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project project = new Project();
        @NotNull final String userId = USER_ID_1;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Project Description";
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project actualProject = projectRepository.findOneById(project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(userId, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project TEST ADD # " + i);
            project.setDescription("Description  TEST ADD #" + i);
            project.setUserId(USER_ID_2);
            projectList.add(project);
        }
        projectRepository.addAll(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testExistsByIdPositive() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String id = project.getId();
            Assert.assertTrue(projectRepository.existsById(id));
        }
    }

    @Test
    public void testExistsByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsById(id));
    }

    @Test
    public void testExistByIdForUserPositive() {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            Assert.assertTrue(projectRepository.existsById(userId, id));
        }
    }

    @Test
    public void testExistByIdForUserNegative() {
        for (@NotNull final Project project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = UUID.randomUUID().toString();
            Assert.assertFalse(projectRepository.existsById(userId, id));
        }
    }

    @Test
    public void testFindOneByIdPositive() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.findOneById(project.getId()));
            Assert.assertEquals(project, projectRepository.findOneById(project.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findOneById(id));
    }

    @Test
    public void testFindOneByIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(projectRepository.findOneById(USER_ID_1, project.getId()));
                Assert.assertEquals(project, projectRepository.findOneById(USER_ID_1, project.getId()));
            } else Assert.assertNull(projectRepository.findOneById(USER_ID_1, project.getId()));
        }
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Assert.assertEquals(projectList.get(i), projectRepository.findOneByIndex(i));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() throws IndexOutOfBoundsException {
        projectRepository.findOneByIndex(NUMBER_OF_ENTRIES + 1);
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(expected1, projectRepository.findOneByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, projectRepository.findOneByIndex(USER_ID_2, 0));
        Assert.assertNull(projectRepository.findOneByIndex("unknown_user_id", 0));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll();
        Assert.assertEquals(projectList, actualProjectList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll(USER_ID_1);
        Assert.assertEquals(projectList.subList(0, NUMBER_OF_ENTRIES / 2), actualProjectList);
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Project project : projectList) {
            projectRepository.remove(project);
            Assert.assertNull(projectRepository.findOneById(project.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Project project = new Project();
        projectRepository.remove(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testRemoveOnePositive() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = projectList.get(i);
            projectRepository.removeOne(project.getUserId(), project);
            Assert.assertEquals(projectRepository.getSize(), NUMBER_OF_ENTRIES - i - 1);
        }
    }

    @Test
    public void testRemoveOneNegative() {
        @NotNull final Project project = new Project();
        projectRepository.removeOne(USER_ID_1, project);
        Assert.assertEquals(projectRepository.getSize(), NUMBER_OF_ENTRIES);
    }

    @Test
    public void testRemoveAll() {
        final int expectedNumberOfEntries = 0;
        projectRepository.removeAll();
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveAllForUserPositive() {
        @NotNull final List<Project> emptyList = new ArrayList<>();
        projectRepository.removeAllByUserId(USER_ID_1);
        Assert.assertEquals(emptyList, projectRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(emptyList, projectRepository.findAll(USER_ID_2));
    }

    @Test
    public void testRemoveAllForUserNegative() {
        projectRepository.removeAllByUserId("Other_user_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testRemoveOneByIdPositive() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.removeOneById(project.getId()));
            Assert.assertNull(projectRepository.findOneById(project.getId()));
        }
    }

    @Test
    public void testRemoveOneByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.removeOneById(id));
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testRemoveOneByIndexPositive() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            projectRepository.removeOneByIndex(0);
            Assert.assertEquals(NUMBER_OF_ENTRIES - i, projectRepository.getSize());
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveOneByIndexNegative() {
        projectRepository.removeOneByIndex(NUMBER_OF_ENTRIES + 1);
    }

}
