package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.repository.IUserOwnedRepository;
import ru.t1.kharitonova.tm.api.service.IUserOwnedService;
import ru.t1.kharitonova.tm.exception.field.IdEmptyException;
import ru.t1.kharitonova.tm.exception.field.IndexIncorrectException;
import ru.t1.kharitonova.tm.exception.field.UserIdEmptyException;
import ru.t1.kharitonova.tm.exception.user.AccessDeniedException;
import ru.t1.kharitonova.tm.model.AbstractUserOwnedModel;
import ru.t1.kharitonova.tm.model.User;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R>
        implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        repository.removeAllByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(userId, id);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        return repository.add(userId, model);
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (model == null) return null;
        return repository.removeOne(userId, model);
    }

}
