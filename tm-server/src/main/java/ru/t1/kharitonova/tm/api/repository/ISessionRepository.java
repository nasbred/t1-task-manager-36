package ru.t1.kharitonova.tm.api.repository;

import ru.t1.kharitonova.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
