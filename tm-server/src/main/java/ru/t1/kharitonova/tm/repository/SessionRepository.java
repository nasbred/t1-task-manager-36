package ru.t1.kharitonova.tm.repository;

import ru.t1.kharitonova.tm.api.repository.ISessionRepository;
import ru.t1.kharitonova.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session>
        implements ISessionRepository {

}
