package ru.t1.kharitonova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.endpoint.*;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ISessionRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.endpoint.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.SessionRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;
import ru.t1.kharitonova.tm.repository.UserRepository;
import ru.t1.kharitonova.tm.service.*;
import ru.t1.kharitonova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ICalculatorEndpoint calculatorEndpoint = new CalculatorEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    @Getter
    private final Backup backup = new Backup(this);

    {
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(calculatorEndpoint);
        registry(authEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }


    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User userTest = userService.create("test", "test", "test@test.ru");
        @NotNull final User userUser = userService.create("user", "user", "user@user.ru");
        @NotNull final User userAdmin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userTest.getId(), "TEST PROJECT 2", "UserTest Project 2");
        projectService.create(userTest.getId(), "TEST PROJECT 1", "UserTest Project 1");
        projectService.create(userUser.getId(), "TEST PROJECT 3", "UserUser Project 3");
        projectService.create(userUser.getId(), "TEST PROJECT 4", "UserUser Project 4");
        projectService.create(userAdmin.getId(), "TEST PROJECT 5", "UserAdmin Project 5");
        projectService.create(userAdmin.getId(), "TEST PROJECT 6", "UserAdmin Project 6");

        taskService.create(userTest.getId(), "TEST TASK 1", "DESCRIPTION 1");
        taskService.create(userTest.getId(), "TEST TASK 2", "DESCRIPTION 2");
        taskService.create(userUser.getId(), "TEST TASK 3", "DESCRIPTION 3");
        taskService.create(userUser.getId(), "TEST TASK 4", "DESCRIPTION 4");
        taskService.create(userAdmin.getId(), "TEST TASK 5", "DESCRIPTION 5");
        taskService.create(userAdmin.getId(), "TEST TASK 6", "DESCRIPTION 6");
    }

    public void start() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER**");
        backup.init();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    public void stop() {
        backup.stop();
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}
