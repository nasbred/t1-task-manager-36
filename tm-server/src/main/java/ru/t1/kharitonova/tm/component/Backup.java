package ru.t1.kharitonova.tm.component;

import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    public static final String FILE_BACKUP = "./tm-client/backup.base64";

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void save() {
        bootstrap.getDomainService().saveDataBackup();
    }

    public void load() {
        if (!Files.exists(Paths.get(FILE_BACKUP))) return;
        bootstrap.getDomainService().loadDataBackup();
    }

    public void stop() {
        es.shutdown();
    }

}
