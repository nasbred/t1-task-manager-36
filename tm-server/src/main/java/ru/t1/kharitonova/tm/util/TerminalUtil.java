package ru.t1.kharitonova.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner scanner = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return scanner.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final Exception e) {
            throw new NumberIncorrectException(value, e);
        }
    }

}
