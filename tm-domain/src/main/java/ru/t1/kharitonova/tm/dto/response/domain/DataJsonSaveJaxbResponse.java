package ru.t1.kharitonova.tm.dto.response.domain;

import lombok.NoArgsConstructor;
import ru.t1.kharitonova.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class DataJsonSaveJaxbResponse extends AbstractResponse {
}