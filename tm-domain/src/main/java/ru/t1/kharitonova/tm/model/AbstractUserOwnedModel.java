package ru.t1.kharitonova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @NotNull
    private String userId;

    public AbstractUserOwnedModel(@NotNull final String userId) {
        this.userId = userId;
    }

}
