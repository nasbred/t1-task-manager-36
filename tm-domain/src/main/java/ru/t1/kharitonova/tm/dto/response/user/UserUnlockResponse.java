package ru.t1.kharitonova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.response.AbstractResultResponse;
import ru.t1.kharitonova.tm.dto.response.AbstractUserResponse;
import ru.t1.kharitonova.tm.model.User;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractUserResponse {

    public UserUnlockResponse(@Nullable final User user) {
        super(user);
    }

}
