package ru.t1.kharitonova.tm.exception.user;

import ru.t1.kharitonova.tm.exception.AbstractException;

public class PermissionDeniedException extends AbstractException {

    public PermissionDeniedException() {
        super("Error! Permission denied.");
    }

}
