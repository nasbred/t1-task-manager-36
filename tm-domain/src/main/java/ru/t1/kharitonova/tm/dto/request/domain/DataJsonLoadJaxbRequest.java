package ru.t1.kharitonova.tm.dto.request.domain;

import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractUserRequest;

public final class DataJsonLoadJaxbRequest extends AbstractUserRequest {

    public DataJsonLoadJaxbRequest(@Nullable final String token) {
        super(token);
    }

}
