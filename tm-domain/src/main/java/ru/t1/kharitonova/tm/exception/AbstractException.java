package ru.t1.kharitonova.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public abstract class AbstractException extends RuntimeException {

    public AbstractException(@NotNull final String message) {
        super(message);
    }

    public AbstractException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractException(@NotNull final String message,
                             @NotNull final Throwable cause,
                             final boolean enableSuppression,
                             final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
