package ru.t1.kharitonova.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.user.*;
import ru.t1.kharitonova.tm.dto.response.user.*;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint extends IEndPoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndPoint.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndPoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request);

    @NotNull
    UserProfileResponse viewProfileUser(@NotNull UserProfileRequest request);

    @NotNull
    UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request);

}
