package ru.t1.kharitonova.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.response.AbstractResponse;
import ru.t1.kharitonova.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListResponse extends AbstractResponse {

    @Nullable
    private List<Task> tasks;

    public TaskListResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}