package ru.t1.kharitonova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kharitonova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kharitonova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.kharitonova.tm.dto.request.task.*;
import ru.t1.kharitonova.tm.dto.request.user.UserLoginRequest;
import ru.t1.kharitonova.tm.dto.response.task.TaskCreateResponse;
import ru.t1.kharitonova.tm.dto.response.task.TaskListResponse;
import ru.t1.kharitonova.tm.dto.response.task.TaskShowByIdResponse;
import ru.t1.kharitonova.tm.dto.response.task.TaskUpdateByIdResponse;
import ru.t1.kharitonova.tm.dto.response.user.UserLoginResponse;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.marker.SoapCategory;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @Nullable
    private String token;

    @Before
    public void initTest() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        token = loginResponse.getToken();
    }

    @Test
    public void testBindTaskToProject() {
        @NotNull final TaskCreateRequest requestCreateTask = new TaskCreateRequest(
                token, "Task bind test", "Task bind test");
        taskEndpoint.createTask(requestCreateTask);
        @NotNull final String taskId = taskEndpoint.createTask(requestCreateTask).getTask().getId();
        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(
                token, "Project bind test", "Project bind test");
        Assert.assertNotNull(projectEndpoint.createProject(requestCreateProject).getProject());
        @NotNull final String projectId = projectEndpoint.createProject(requestCreateProject).getProject().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(
                token, projectId, taskId
        );
        taskEndpoint.bindTaskToProject(request);

        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(projectId, taskList.get(1).getProjectId());
    }

    @Test
    public void testUnbindTaskFromProject() {
        @NotNull final TaskCreateRequest requestCreateTask = new TaskCreateRequest(
                token, "Task unbind test", "Task unbind test");
        taskEndpoint.createTask(requestCreateTask);
        Assert.assertNotNull(taskEndpoint.createTask(requestCreateTask).getTask());
        @NotNull final String taskId = taskEndpoint.createTask(requestCreateTask).getTask().getId();

        @NotNull final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(
                token, "Project unbind test", "Project unbind test");
        @NotNull final String projectId = projectEndpoint.createProject(requestCreateProject).getProject().getId();

        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(
                token, projectId, taskId
        );
        taskEndpoint.bindTaskToProject(requestBind);

        @NotNull final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(
                token, projectId, taskId
        );
        taskEndpoint.unbindTaskToProject(requestUnbind);

        @NotNull final TaskListRequest requestList = new TaskListRequest(token);
        @NotNull TaskListResponse responseList = taskEndpoint.listTask(requestList);
        @Nullable List<Task> taskList = responseList.getTasks();
        Assert.assertNotNull(taskList);
        Assert.assertNull(taskList.get(0).getProjectId());
    }

    @Test
    public void testChangeTaskStatusById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test Task", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable Task task = response.getTask();
        Assert.assertNotNull(task);
        @Nullable final String taskId = task.getId();
        @Nullable final Status status = task.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        @NotNull final Status newStatus = Status.IN_PROGRESS;
        task = taskEndpoint.changeStatusById(new TaskChangeStatusByIdRequest(token, taskId, newStatus)).getTask();
        Assert.assertNotNull(task);
        Assert.assertNotEquals(Status.NOT_STARTED, task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testClearTask() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskEndpoint.createTask(new TaskCreateRequest(token, "Test task", "Test Description"));
        @Nullable List<Task> tasks = taskEndpoint.listTask(new TaskListRequest(token)).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        taskEndpoint.clearTask(new TaskClearRequest(token));
        tasks = taskEndpoint.listTask(new TaskListRequest(token)).getTasks();
        Assert.assertNull(tasks);
    }

    @Test
    public void testCreateTask() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test task", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable Task task = response.getTask();
        Assert.assertNotNull(task);
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testShowTaskById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test task", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable Task task = response.getTask();
        Assert.assertNotNull(task);
        @NotNull String taskId = task.getId();
        @NotNull final TaskShowByIdResponse taskGetByIdResponse = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(token, taskId)
        );
        Assert.assertNotNull(taskGetByIdResponse);
        Assert.assertNotNull(taskGetByIdResponse.getTask());
        Assert.assertEquals("Test task", taskGetByIdResponse.getTask().getName());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testListTask() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskEndpoint.createTask(new TaskCreateRequest(token, "Test1 task", "Test Description"));
        taskEndpoint.createTask(new TaskCreateRequest(token, "Test2 task", "Test Description2"));
        @Nullable List<Task> tasks = taskEndpoint.listTask(new TaskListRequest(token)).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testListTaskByProjectId() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskEndpoint.createTask(new TaskCreateRequest(token, "Test1 task", "Test Description"));
        taskEndpoint.createTask(new TaskCreateRequest(token, "Test2 task", "Test Description2"));
        @Nullable List<Task> tasks = taskEndpoint.listTask(new TaskListRequest(token)).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testRemoveTaskById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskEndpoint.createTask(new TaskCreateRequest(
                token, "First task", "Task Description"));
        @Nullable List<Task> tasks = taskEndpoint.listTask(new TaskListRequest(token)).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        @NotNull final String taskId1 = tasks.get(0).getId();
        taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(token, taskId1));
        tasks = taskEndpoint.listTask(new TaskListRequest(token)).getTasks();
        Assert.assertNull(tasks);
    }

    @Test
    public void testUpdateTaskById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test task", "Test Description")
        );
        @Nullable Task task = response.getTask();
        Assert.assertNotNull(task);
        @Nullable String taskId = task.getId();
        Assert.assertEquals("Test task", task.getName());
        @NotNull final TaskUpdateByIdResponse taskUpdateByIdResponse = taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(token, taskId, "Test task new", "Test Description New")
        );
        Assert.assertNotNull(taskUpdateByIdResponse);
        Assert.assertNotNull(taskUpdateByIdResponse.getTask());
        Assert.assertEquals(taskId, taskUpdateByIdResponse.getTask().getId());
        Assert.assertEquals("Test task new", taskUpdateByIdResponse.getTask().getName());
        Assert.assertEquals("Test Description New", taskUpdateByIdResponse.getTask().getDescription());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testStartTaskById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test Task", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable Task task = response.getTask();
        Assert.assertNotNull(task);
        @Nullable final String taskId = task.getId();
        @Nullable final Status status = task.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        task = taskEndpoint.startTaskById(new TaskStartByIdRequest(token, taskId)).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

    @Test
    public void testCompleteTaskById() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(token, "Test Task", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable Task task = response.getTask();
        Assert.assertNotNull(task);
        @Nullable final String taskId = task.getId();
        @Nullable final Status status = task.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        task = taskEndpoint.completeTaskById(new TaskCompleteByIdRequest(token, taskId)).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
        taskEndpoint.clearTask(new TaskClearRequest(token));
    }

}
