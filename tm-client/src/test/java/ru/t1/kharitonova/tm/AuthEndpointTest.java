package ru.t1.kharitonova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.dto.request.user.UserLoginRequest;
import ru.t1.kharitonova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.kharitonova.tm.dto.request.user.UserProfileRequest;
import ru.t1.kharitonova.tm.dto.response.user.UserLoginResponse;
import ru.t1.kharitonova.tm.dto.response.user.UserLogoutResponse;
import ru.t1.kharitonova.tm.dto.response.user.UserProfileResponse;
import ru.t1.kharitonova.tm.marker.SoapCategory;
import ru.t1.kharitonova.tm.service.PropertyService;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort());

    @Test
    public void testLogin() {
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(
                new UserLoginRequest(null, null))
        );
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
    }

    @Test
    public void testLogout() {
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        @NotNull final UserLogoutResponse userLogoutResponse = authEndpoint.logout(
                new UserLogoutRequest(userLoginResponse.getToken())
        );
        Assert.assertNotNull(userLogoutResponse);
    }

    @Test
    public void testProfile() {
        @Nullable final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        @Nullable final UserProfileResponse userProfileResponse = authEndpoint.profile(
                new UserProfileRequest(userLoginResponse.getToken())
        );
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
    }

}
