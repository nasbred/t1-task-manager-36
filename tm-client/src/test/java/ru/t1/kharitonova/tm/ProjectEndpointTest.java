package ru.t1.kharitonova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kharitonova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.kharitonova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.dto.request.project.*;
import ru.t1.kharitonova.tm.dto.request.user.UserLoginRequest;
import ru.t1.kharitonova.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.kharitonova.tm.dto.response.project.ProjectShowByIdResponse;
import ru.t1.kharitonova.tm.dto.response.project.ProjectUpdateByIdResponse;
import ru.t1.kharitonova.tm.dto.response.user.UserLoginResponse;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.marker.SoapCategory;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(
            propertyService.getHost(), propertyService.getPort()
    );

    @Nullable
    private String token;

    @Before
    public void initTest() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        token = loginResponse.getToken();
    }

    @Test
    public void testChangeProjectStatusById() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        @Nullable final String projectId = project.getId();
        @Nullable final Status status = project.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        @NotNull final Status newStatus = Status.IN_PROGRESS;
        project = projectEndpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, projectId, newStatus)
        ).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testClearProject() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        @Nullable List<Project> projects = projectEndpoint.listProject(
                new ProjectListRequest(token)
        ).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        projects = projectEndpoint.listProject(new ProjectListRequest(token)).getProjects();
        Assert.assertNull(projects);
    }

    @Test
    public void testCreateProject() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testShowProjectById() {
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        @NotNull String projectId = project.getId();
        @NotNull final ProjectShowByIdResponse projectGetByIdResponse = projectEndpoint.showProjectById(
                new ProjectShowByIdRequest(token, projectId)
        );
        Assert.assertNotNull(projectGetByIdResponse);
        Assert.assertNotNull(projectGetByIdResponse.getProject());
        Assert.assertEquals("Test Project", projectGetByIdResponse.getProject().getName());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testListProject() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        projectEndpoint.createProject(new ProjectCreateRequest(
                token, "Test Project", "Test Description")
        );
        projectEndpoint.createProject(new ProjectCreateRequest(
                token, "Test2 Project", "Test2 Description2")
        );
        @Nullable List<Project> projects = projectEndpoint.listProject(
                new ProjectListRequest(token)).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testRemoveProjectById() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        projectEndpoint.createProject(new ProjectCreateRequest(
                token, "Test Project", "Test Description")
        );
        @Nullable List<Project> projects = projectEndpoint.listProject(
                new ProjectListRequest(token)).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
        @NotNull final String projectId1 = projects.get(0).getId();
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, projectId1));
        projects = projectEndpoint.listProject(new ProjectListRequest(token)).getProjects();
        Assert.assertNull(projects);
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testUpdateProjectById() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        @Nullable String projectId = project.getId();
        Assert.assertEquals("Test Project", project.getName());
        @NotNull final ProjectUpdateByIdResponse projectUpdateByIdResponse = projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(token, projectId, "Test Project new", "Test Description New")
        );
        Assert.assertNotNull(projectUpdateByIdResponse);
        Assert.assertNotNull(projectUpdateByIdResponse.getProject());
        Assert.assertEquals(projectId, projectUpdateByIdResponse.getProject().getId());
        Assert.assertEquals("Test Project new", projectUpdateByIdResponse.getProject().getName());
        Assert.assertEquals("Test Description New", projectUpdateByIdResponse.getProject().getDescription());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testStartProjectById() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        @Nullable final String projectId = project.getId();
        @Nullable final Status status = project.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        project = projectEndpoint.startProjectById(
                new ProjectStartByIdRequest(token, projectId)
        ).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

    @Test
    public void testCompleteProjectById() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "Test Project", "Test Description")
        );
        Assert.assertNotNull(response);
        @Nullable Project project = response.getProject();
        Assert.assertNotNull(project);
        @Nullable final String projectId = project.getId();
        @Nullable final Status status = project.getStatus();
        Assert.assertEquals(Status.NOT_STARTED, status);
        project = projectEndpoint.completeProjectById(new ProjectCompleteByIdRequest(token, projectId)).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
        projectEndpoint.clearProject(new ProjectClearRequest(token));
    }

}
