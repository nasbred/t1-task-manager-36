package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.kharitonova.tm.command.AbstractCommand;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return serviceLocator.getTaskEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(@Nullable final List<Task> tasks) {
        if (tasks == null || tasks.size() == 0) return;
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("PROJECT ID: " + task.getProjectId());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
