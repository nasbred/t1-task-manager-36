package ru.t1.kharitonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.user.UserLoginRequest;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Login user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        @Nullable final String token = getAuthEndpoint().login(request).getToken();
        setToken(token);
        System.out.println(token);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
