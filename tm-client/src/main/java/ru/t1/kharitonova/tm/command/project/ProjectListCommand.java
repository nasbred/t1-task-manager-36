package ru.t1.kharitonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.project.ProjectListRequest;
import ru.t1.kharitonova.tm.enumerated.ProjectSort;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show project list.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);

        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sort);
        @Nullable final List<Project> projects = getProjectEndpoint().listProject(request).getProjects();
        if (projects == null) return;
        int index = 1;
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + " : " + project.getStatus());
            index++;
        }
    }

}
